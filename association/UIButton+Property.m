//
//  UIButton+Property.m
//  association
//
//  Created by hibo on 2018/6/28.
//  Copyright © 2018年 hibo. All rights reserved.
//

#import "UIButton+Property.h"
#import <objc/runtime.h>

const NSString *button_property_ids = @"button_property_ids";
const NSString *button_property_click_event = @"button_property_click_event";
@implementation UIButton (Property)
-(instancetype)init{
    self = [super init];
    if (self) {
       NSLog(@"初始化");
    }
    return self;
}
-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        NSLog(@"初始化");
        [self addTarget:self action:@selector(clickBtnEvent:) forControlEvents:UIControlEventTouchUpInside];
    }
    return self;
}
-(void)setIds:(NSString *)ids{
    objc_setAssociatedObject(self, &button_property_ids, ids, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
-(NSString *)ids{
    return objc_getAssociatedObject(self, &button_property_ids);
}
-(void)setClickBlock:(ButtonEventBlock)clickBlock{
    objc_setAssociatedObject(self, &button_property_click_event, clickBlock, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
-(ButtonEventBlock)clickBlock{
    return objc_getAssociatedObject(self, &button_property_click_event);
}
//击事件中调用方法把点击事件通过代码块传递出去
-(void)clickBtnEvent:(UIButton *)button{
    ButtonEventBlock block = [self clickBlock];
    if (block) {
        block(button);
    }
}

@end
