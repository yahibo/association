//
//  UIButton+Property.h
//  association
//
//  Created by hibo on 2018/6/28.
//  Copyright © 2018年 hibo. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^ButtonEventBlock)(UIButton *response);
@interface UIButton (Property)
@property (nonatomic,strong) NSString *ids;
@property (nonatomic,strong) ButtonEventBlock clickBlock;

@end
