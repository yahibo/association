//
//  AppDelegate.h
//  association
//
//  Created by hibo on 2018/6/28.
//  Copyright © 2018年 hibo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

